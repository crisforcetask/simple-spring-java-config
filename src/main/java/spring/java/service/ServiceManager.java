/**
 * 
 */
package spring.java.service;

import spring.java.domain.BlogPost;

/**
 * @author cristina.smaranda
 *
 */
public interface ServiceManager {

	void sendBlogPost(BlogPost blogPost);
}
