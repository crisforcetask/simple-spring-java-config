/**
 * 
 */
package spring.java.service;

import spring.java.domain.BlogPost;

/**
 * @author cristina.smaranda
 *
 */
public interface BlogPostService {

	void savePost(BlogPost blogPost);
}
