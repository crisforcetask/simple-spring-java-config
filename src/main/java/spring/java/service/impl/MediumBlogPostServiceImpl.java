/**
 * 
 */
package spring.java.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import spring.java.domain.BlogPost;
import spring.java.service.BlogPostService;

/**
 * @author cristina.smaranda
 *
 */
public class MediumBlogPostServiceImpl implements BlogPostService {

	// the logger for this class
	private static final Logger LOGGER = LoggerFactory.getLogger(MediumBlogPostServiceImpl.class);

	// class name
	public static final String CLASS_NAME = MediumBlogPostServiceImpl.class.getName();

	@Override
	public void savePost(BlogPost blogPost) {

		final String METHOD_NAME = "savePost";
		LOGGER.debug(CLASS_NAME + METHOD_NAME + " ----- START -------");

	}

}
