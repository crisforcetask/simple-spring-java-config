/**
 * 
 */
package spring.java.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import spring.java.domain.BlogPost;
import spring.java.service.BlogPostService;
import spring.java.service.ServiceManager;

/**
 * @author cristina.smaranda
 *
 */
public class ServiceManagerImpl implements ServiceManager {

	private static final Logger LOGGER = LoggerFactory.getLogger(ServiceManagerImpl.class);
	
	@Autowired
	@Qualifier("WordPressBlodPressSerice")
	private BlogPostService blogPostService;
	/* (non-Javadoc)
	 * @see spring.java.service.ServiceManager#sendBlogPost(spring.java.domain.BlogPost)
	 */
	@Override
	public void sendBlogPost(BlogPost blogPost) {
		LOGGER.debug(" ce trimit aici? ");
		blogPostService.savePost(blogPost);

	}

}
