/**
 * 
 */
package spring.java.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import spring.java.domain.BlogPost;
import spring.java.domain.DataSource;
import spring.java.service.BlogPostService;
import spring.java.service.EmailService;

/**
 * @author cristina.smaranda
 *
 */
public class BlogPostServiceImpl implements BlogPostService {

	// the logger for this class
	private static final Logger LOGGER = LoggerFactory.getLogger(BlogPostServiceImpl.class);

	// class name
	public static final String CLASS_NAME = BlogPostService.class.getName();

	@Autowired
	private DataSource dataSource;

	@Autowired
	private EmailService emailService;

	@Override
	public void savePost(BlogPost blogPost) {

		final String METHOD_NAME = "savePost";
		LOGGER.debug(CLASS_NAME + METHOD_NAME + " ----- START -------");
		LOGGER.debug(CLASS_NAME + METHOD_NAME + " ds=" + dataSource.getUsername());
		emailService.sendEmail();
	}

}
