/**
 * 
 */
package spring.java.service;

/**
 * @author cristina.smaranda
 *
 */
public interface EmailService {

	void sendEmail();
}
