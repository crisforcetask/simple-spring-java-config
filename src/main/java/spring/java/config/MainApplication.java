/**
 * 
 */
package spring.java.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import spring.java.domain.BlogPost;
import spring.java.domain.DataSource;
import spring.java.service.BlogPostService;
import spring.java.service.EmailService;
import spring.java.service.ServiceManager;

/**
 * @author cristina.smaranda
 *
 */
public class MainApplication {

	private static final Logger LOGGER = LoggerFactory.getLogger(MainApplication.class);
	public static void main(String[] args) {

		/**
		 * For creating the application context, instead of providing the
		 * <code>application-context.xml</code> file we provide the java class
		 * name used for configure our application.
		 */
		ApplicationContext applicationContext = new AnnotationConfigApplicationContext(JavaConfig.class);

		// you can create an object by using the class or by method name. In
		// case you are using the method name the cast is needed
//		@Autowired
//		@Qualifier("MediumBlogPostService")
//		BlogPostService blogPostService;
		BlogPostService blogPostService_methodName = (BlogPostService) applicationContext.getBean("blogPostService");
		BlogPost blogPost = new BlogPost();
		blogPost.setId(1L);
		blogPost.setTitle("Java Spring MVC Java Config");

		//blogPostService.savePost(blogPost);
		
		blogPostService_methodName.savePost(blogPost);
		
		DataSource dataSource = applicationContext.getBean(DataSource.class);
		LOGGER.debug("DS url: " + dataSource.getUrl());
		
		EmailService emailService = applicationContext.getBean(EmailService.class);
		emailService.sendEmail();
		
		ServiceManager serviceManager = applicationContext.getBean(ServiceManager.class);
		
		serviceManager.sendBlogPost(blogPost);
		
		
		// close the application context
		((ConfigurableApplicationContext) applicationContext).close();
	}
}
