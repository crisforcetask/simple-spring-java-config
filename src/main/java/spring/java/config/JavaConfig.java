/**
 * 
 */
package spring.java.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import spring.config.ServiceConfig;
import spring.java.service.BlogPostService;
import spring.java.service.impl.BlogPostServiceImpl;
import spring.java.service.impl.MediumBlogPostServiceImpl;
import spring.java.service.impl.WordPressBlogPostServiceImpl;

/**
 * Java Configuration file instead of using the
 * <code>application-config.xml</code> file to configure our java application
 * The <code>beans</code> usually defined into <code>config.xml</code> file are
 * going to be defined into this class. The class will be passed at parameter
 * for instantiating the application see{@link MainApplication}
 *
 * 
 * @author cristina.smaranda
 *
 */
@ComponentScan(basePackages = { "spring.java" })
@Import(value = { ServiceConfig.class })
/**
 * in case you want to add to your application context other 'config' files that
 * are not under the packages you mentioned for @ComponentScan you include those
 * classes here by using the annotation @Import see {@link ServiceConfig}
 */
public class JavaConfig {

	public static final String CLASS_NAME = JavaConfig.class.getName();

	@Autowired
	private DBConfig dbConfig;
	// configure the service. The default name of the bean is the method name
	
	@Bean
	public BlogPostService blogPostService() {
		return new BlogPostServiceImpl();
	}
	
	@Bean(name="WordPressBlodPressSerice")
	public BlogPostService wordPressBlogPostService() {
		return new WordPressBlogPostServiceImpl();
	}
	
	@Bean(name="MediumBlogPostService")
	public BlogPostService mediumBlogPostService() {
		return new MediumBlogPostServiceImpl();
	}
}
